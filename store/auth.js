export const state = () => ({
  token: null,
  role: null
})
export const mutations = {
  setToken(state, token) {
    state.token = token
  },
  setRole(state, role) {
    state.role = role
  }
}