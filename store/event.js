export const state = () => ({
  id: null,
})
export const mutations = {
  setEventId(state, id) {
    state.id = id
  },
}