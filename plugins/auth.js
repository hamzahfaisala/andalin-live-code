import { NhostClient } from '@nhost/nhost-js'

const nhost = new NhostClient({
    backendUrl: process.env.backendUrl,
  })

export default function () {
  if (!nhost.auth.isAuthenticated()) {
    this.$router.push('/auth');
  }
}
