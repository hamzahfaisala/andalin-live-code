export default (ctx) => {
  return {
    httpEndpoint: "https://fosyurgrcqlnwoenmtxs.hasura.eu-central-1.nhost.run/v1/graphql",
    wsEndpoint: "wss://fosyurgrcqlnwoenmtxs.hasura.eu-central-1.nhost.run/v1/graphql",
    httpLinkOptions: {
      headers: {
        "x-hasura-admin-secret": "O=Io*KymH)2q4@E@tk8yOOGgT06(U2mZ",
      },
    },
    getAuth: () => {
      const token = ctx.$nhost.auth.getJWTToken();
      return token ? `Bearer ${token}` : null;
    },
  };
};
