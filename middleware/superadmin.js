import { NhostClient } from '@nhost/nhost-js'

const nhost = new NhostClient({
  backendUrl: process.env.backendUrl,
})

export default async function ({redirect, store,}) {
    var isAuthenticated = await nhost.auth.isAuthenticatedAsync()
    if (isAuthenticated) {
        var role = await nhost.auth.getUser().defaultRole
        if (role == 'me') {
            // console.log(role);
            // await store.commit('auth/setRole', role)
            return
        }else{
            // await nhost.auth.signOut()
            // redirect({name: 'auth'}) 
            // store.commit('auth/setRole', null)
        }
    }else{
        // await nhost.auth.signOut()
        // store.commit('auth/setRole', null)
        redirect({name: 'auth'})    
    }
}