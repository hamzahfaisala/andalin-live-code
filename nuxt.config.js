import colors from "vuetify/es5/util/colors";

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  ssr: false,
  // target: 'server',
  head: {
    titleTemplate: "%s - Muhammadiyah",
    title: "E-Voting",
    htmlAttrs: {
      lang: "en",
    },
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "" },
      { name: "format-detection", content: "telephone=no" },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  // router: {
  //   middleware: ['auth']
  // },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {
      src: "~/plugins/nhost-apollo-ws-client.js",
      mode: "client",
    },
    { src: '~/plugins/apex-chart.js', mode: 'client' },
    { src: '~/plugins/highchart.js', mode: 'client' },
  ],

  

  apollo: {
    clientConfigs: {
      default: "~/plugins/nuxt-apollo-config.js",
    },
  },

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/vuetify
    "@nuxtjs/vuetify",
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nhost/nuxt', 
    '@nuxtjs/apollo', 
    // 'cookie-universal-nuxt',
  ],

  // Nhost configurations
  nhost: {
    backendUrl: "https://fosyurgrcqlnwoenmtxs.nhost.run",
    // optional other nhost-js-sdk setup options
  },

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ["~/assets/variables.scss"],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
      },
    },
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
  env: {
    baseUrl: process.env.BASE_URL || "http://localhost:3000",
    backendUrl: process.env.BACKEND_URL || "https://fosyurgrcqlnwoenmtxs.nhost.run",
    hasuraSecret: process.env.HASURA_SECRET || "O=Io*KymH)2q4@E@tk8yOOGgT06(U2mZ",
  },
};
